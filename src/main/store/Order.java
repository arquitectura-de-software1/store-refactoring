package store;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Order {

	private Customer customer;
	private Salesman salesman;
	private Date orderedOn;
	private String deliveryStreet;
	private String deliveryCity;
	private String deliveryCountry;
	private Set<OrderItem> items;

	public Order(Customer customer, Salesman salesman, String deliveryStreet, String deliveryCity, String deliveryCountry, Date orderedOn) {
		this.customer = customer;
		this.salesman = salesman;
		this.deliveryStreet = deliveryStreet;
		this.deliveryCity = deliveryCity;
		this.deliveryCountry = deliveryCountry;
		this.orderedOn = orderedOn;
		this.items = new HashSet<OrderItem>();
	}

	public Customer getCustomer() {
		return customer;
	}

	public Salesman getSalesman() {
		return salesman;
	}

	public Date getOrderedOn() {
		return orderedOn;
	}

	public String getDeliveryStreet() {
		return deliveryStreet;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public String getDeliveryCountry() {
		return deliveryCountry;
	}

	public Set<OrderItem> getItems() {
		return items;
	}

	public float total() {
		float totalItems = 0;
		
		totalItems = discountCategoryProducts();
		
		if (isDeliveryCountry()){
			// total=totalItems + tax + 0 shipping
			return totalItems + calculateTax(totalItems);
		}
		// total=totalItemst + tax + 15 shipping
		return totalItems + calculateTax(totalItems) + 15;
	}

	private boolean isDeliveryCountry() {
		return this.deliveryCountry == "USA";
	}

	private float discountCategoryProducts() {
		float total = 0;
		float discountProduct = 0;
		for (OrderItem item : items) {
			float itemAmount = item.getProduct().getUnitPrice() * item.getQuantity();
			if (isProductCategoryAccesories(item)) {
				discountProduct = calculateBookDiscount(itemAmount);
			}
			if (isProductCategoryBikes(item)) {
				discountProduct = calculateBikeDiscount(itemAmount);
			}
			if (isProductCategoryClothing(item)) {
				discountProduct = calculateClothingDiscount(item);;
			}
			total += itemAmount - discountProduct;;
		}
		return total;
	}

	private float calculateTax(float totalItems) {
		return totalItems * 5 / 100;
	}
	private boolean isProductCategoryClothing(OrderItem item) {
		return item.getProduct().getCategory() == ProductCategory.Cloathing;
	}
	
	private boolean isProductCategoryBikes(OrderItem item) {
		return item.getProduct().getCategory() == ProductCategory.Bikes;
	}

	private boolean isProductCategoryAccesories(OrderItem item) {
		return item.getProduct().getCategory() == ProductCategory.Accessories;
	}

	private float calculateClothingDiscount(OrderItem item) {
		float cloathingDiscount = 0;
		if (item.getQuantity() > 2) {
			cloathingDiscount = item.getProduct().getUnitPrice();
		}
		return cloathingDiscount;
	}

	private float calculateBikeDiscount(float itemAmount) {
		// 20% discount for Bikes
		return itemAmount * 20 / 100;
	}

	private float calculateBookDiscount(float itemAmount) {
		float booksDiscount = 0;
		if (itemAmount >= 100) {
			booksDiscount = itemAmount * 10 / 100;
		}
		return booksDiscount;
	}
}
